/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entomologia;

/**
 *
 * @author Kath
 */
public class Isoptera extends Entomologia{
    
    private int alas;
    private String castas;
    
    public Isoptera(String reino, String clase, String habitat, String tipos, int patas, int alas, String castas) {
        super(reino, clase, habitat, tipos, patas);
        this.alas = alas;
        this.castas = castas;
    }
    
    public String getAtributos() {
        return "Reino: " + reino
                + "\nClase: " + clase
                + "\nHabitat: " + habitat
                + "\nTipos: " + tipos
                + "\nPatas: " + patas
                +"\nAlas: "+ alas
                +"\nCastas: "+ castas;
    }
    
    //polimorfismo
    public void clase() {
        System.out.println("Pertenece a la clase insecta.");
    }
    
    public void habitat(){
        System.out.println("Habita en bosques tropicales de tierras bajas y sabanas.");
    }
    
    public void tipos(){
        System.out.println("Existen 3 tipos:\n"
                + "1. Termitas de madera húmeda\n"
                + "2. Termitas de madera seca\n"
                + "3. Termitas subterraneas.");
    }
    
}
