/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entomologia;

/**
 *
 * @author Kath
 */
public class Araneae extends Entomologia{
    
    private String seda, depredador;
    
    public Araneae(String reino, String clase, String habitat, String tipos, int patas) {
        super(reino, clase, habitat, tipos, patas);
        this.seda= "sí";
        this.depredador = "sí";
    }
     public String getAtributos() {
        return "Reino: " + reino
                + "\nClase: " + clase
                + "\nHabitat: " + habitat
                + "\nTipos: " + tipos
                + "\nPatas: " + patas
                +"\nSeda: "+ seda
                +"\nDepredador: "+ depredador;
    }
    
    //polimorfismo
    public void clase() {
        System.out.println("Pertenece a la clase arachnida.");
    }
    
    public void habitat(){
        System.out.println("Habita en todo tipo de regiones y climas.");
    }
    
    public void tipos(){
        System.out.println("Existen 3 tipos:\n"
                + "1. Cosmopolita\n"
                + "2. Mycaria\n"
                + "3. Venenosas.\n"
                + "4. Uloboridae.");
    }
    
}
