/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entomologia;

/**
 *
 * @author Kath
 */
public class Entomologia {

    public String reino, clase, habitat, tipos;
    public int patas;

    public Entomologia(String reino, String clase, String habitat, String tipos, int patas) {
        this.reino = reino;
        this.clase = clase;
        this.habitat = habitat;
        this.tipos = tipos;
        this.patas = patas;
    }

    public String getAtributos() {
        return "Reino: " + reino
                + "\nClase: " + clase
                + "\nHabitat: " + habitat
                + "\nTipos: " + tipos
                + "\nPatas: " + patas;
    }
    
    //polimorfismo
    public void clase() {
        System.out.println("¿A qué clase pertenece?");
    }
    
    public void habitat(){
        System.out.println("¿En qué habitat se encuentran?");
    }
    
    public void tipos(){
        System.out.println("¿Cuáles tipos existen?");
    }
}
