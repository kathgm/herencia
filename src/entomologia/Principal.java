/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entomologia;

/**
 *
 * @author Kath
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Entomologia ento = new Entomologia("Animalia", "Insecta/Arachnida", "Varios", "varios",0);

        Isoptera isoptera = new Isoptera("Animalia", "Insecta", "madera", "3",6,4,"Obreros\nSoldados\nRey y Reina\nRey 2\nRey3");
        Araneae araneae = new Araneae("Animalia", "Arachnida", "todo", "4",8);

        System.out.println(ento.getAtributos());
        System.out.println("");
        System.out.println(isoptera.getAtributos());
        tipo(isoptera);
        System.out.println("");
        System.out.println(araneae.getAtributos());
        tipo(araneae);
    }
    
    //polimorfismo
    public static void tipo (Entomologia entomologia){
        entomologia.clase();
        entomologia.habitat();
        entomologia.tipos();
    }
    
}
